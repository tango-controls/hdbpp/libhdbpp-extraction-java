<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <name>HDBExtract</name>
    <description>Java Tango Controls HDB++ Extraction Library</description>
    <url>https://gitlab.com/tango-controls/hdbpp/libhdbpp-extraction-java</url>

    <groupId>org.tango-controls.hdb</groupId>
    <artifactId>HDBExtract</artifactId>
    <version>1.31.2-SNAPSHOT</version>

    <packaging>jar</packaging>

    <licenses>
        <license>
            <name>LGPL-3.0</name>
            <url>https://www.gnu.org/licenses/lgpl-3.0.html</url>
        </license>
    </licenses>

    <developers>
        <developer>
            <id>pons</id>
            <name>Jean Luc Pons</name>
            <email>pons@esrf.fr</email>
            <organization>ESRF</organization>
            <organizationUrl>http://esrf.eu</organizationUrl>
            <roles>
                <role>Maintainer</role>
            </roles>
            <timezone>1</timezone>
        </developer>
        <developer>
            <id>dlacoste-esrf</id>
            <name>Damien Lacoste</name>
            <email>damien.lacoste@esrf.fr</email>
            <organization>ESRF</organization>
            <organizationUrl>http://esrf.eu</organizationUrl>
            <roles>
                <role>Maintainer</role>
            </roles>
            <timezone>1</timezone>
        </developer>
    </developers>

    <scm>
        <connection>scm:git:git@gitlab.com:tango-controls/hdbpp/libhdbpp-extraction-java.git</connection>
        <developerConnection>scm:git:git@gitlab.com:tango-controls/hdbpp/libhdbpp-extraction-java.git</developerConnection>
        <url>https://gitlab.com/tango-controls/hdbpp/libhdbpp-extraction-java</url>
        <tag>HEAD</tag>
    </scm>

    <dependencies>

      <dependency>
          <groupId>com.datastax.cassandra</groupId>
          <artifactId>cassandra-driver-core</artifactId>
          <version>3.11.3</version>
      </dependency>

      <dependency>
        <groupId>com.mysql</groupId>
        <artifactId>mysql-connector-j</artifactId>
        <version>8.0.31</version>
      </dependency>

      <dependency>
          <groupId>org.postgresql</groupId>
          <artifactId>postgresql</artifactId>
          <version>42.5.0</version>
      </dependency>

      <dependency>
        <groupId>org.tango-controls</groupId>
        <artifactId>JTango</artifactId>
        <!-- all versions >= 9.6.8 and < 10.0.0 (version 10.0.0 is not compatible) -->
        <version>[9.6.8, 10.0.0)</version>
        <scope>provided</scope>
        <type>pom</type>
      </dependency>

      <dependency>
          <groupId>fr.esrf.taco</groupId>
          <artifactId>TacoHdb</artifactId>
          <version>3.1</version>
          <scope>provided</scope>
      </dependency>

    </dependencies>

    <build>
        <plugins>
            <!-- call with mvn javadoc:javadoc , generate in target/site/apidocs -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-javadoc-plugin</artifactId>
                <version>3.4.1</version>
                <configuration>
                    <show>public</show>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <version>3.10.1</version>
                <configuration>
                    <compilerArgument>-Xlint:all</compilerArgument>
                    <showWarnings>true</showWarnings>
                    <showDeprecation>true</showDeprecation>
                    <encoding>UTF-8</encoding>
                </configuration>
            </plugin>
            <plugin>
                <artifactId>maven-release-plugin</artifactId>
                <version>2.5.3</version>
                <configuration>
                    <useReleaseProfile>false</useReleaseProfile>
                    <releaseProfiles>release</releaseProfiles>
                    <autoVersionSubmodules>true</autoVersionSubmodules>
                </configuration>
            </plugin>
	<!-- this builds fat-jar -->
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <id>assembly</id>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                        <configuration>
                            <appendAssemblyId>false</appendAssemblyId>
                            <descriptorRefs>
                                <descriptorRef>jar-with-dependencies</descriptorRef>
                            </descriptorRefs>
                            <archive>
                                <manifest>
                                    <mainClass>org.tango.jhdb.Hdb</mainClass>
                                    <addDefaultImplementationEntries>true</addDefaultImplementationEntries>
                                    <addDefaultSpecificationEntries>true</addDefaultSpecificationEntries>
                                </manifest>
                            </archive>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>

    <profiles>
        <profile>
            <id>release</id>
            <build>
                <plugins>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-source-plugin</artifactId>
                        <version>3.2.1</version>
                        <executions>
                            <execution>
                                <id>attach-sources</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-javadoc-plugin</artifactId>
                        <version>3.4.1</version>
                        <executions>
                            <execution>
                                <id>attach-javadocs</id>
                                <goals>
                                    <goal>jar</goal>
                                </goals>
                                <configuration>
                                    <doclint>none,-syntax</doclint>
                                </configuration>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.apache.maven.plugins</groupId>
                        <artifactId>maven-gpg-plugin</artifactId>
                        <version>3.0.1</version>
                        <executions>
                            <execution>
                                <id>sign-artifacts</id>
                                <phase>verify</phase>
                                <goals>
                                    <goal>sign</goal>
                                </goals>
                            </execution>
                        </executions>
                    </plugin>
                    <plugin>
                        <groupId>org.codehaus.mojo</groupId>
                        <artifactId>versions-maven-plugin</artifactId>
                        <version>2.10.0</version>
                    </plugin>
                    <plugin>
                        <groupId>org.sonatype.plugins</groupId>
                        <artifactId>nexus-staging-maven-plugin</artifactId>
                        <version>1.6.12</version>
                        <extensions>true</extensions>
                        <configuration>
                            <serverId>ossrh</serverId>
                            <nexusUrl>https://oss.sonatype.org/</nexusUrl>
                            <autoReleaseAfterClose>true</autoReleaseAfterClose>
                        </configuration>
                    </plugin>
                </plugins>
            </build>
        </profile>
    </profiles>

    <!-- TODO gitlab release -->
    <distributionManagement>
        <snapshotRepository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/content/repositories/snapshots</url>
        </snapshotRepository>
        <repository>
            <id>ossrh</id>
            <url>https://oss.sonatype.org/service/local/staging/deploy/maven2/</url>
        </repository>
    </distributionManagement>

    <repositories>
        <repository>
            <id>local-contrib</id>
            <url>file://${project.basedir}/contrib</url>
        </repository>
        <repository>
            <id>mvnrepository</id>
            <url>https://mvnrepository.com/artifact</url>
        </repository>

	</repositories>	

</project>
